package omgcode.kafkaservice.service.kafka;


import lombok.extern.slf4j.Slf4j;
import omgcode.kafkaservice.payload.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JsonKafkaConsumer {

    @KafkaListener(topics = "omgJson", groupId = "myGroup")
    public void consume(User data) {
        log.info(String.format("Json message format -> %s", data.toString()));
    }
}
