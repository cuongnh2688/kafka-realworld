package omgcode.kafkaservice.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic omgTopic() {
        return TopicBuilder.name("omg")
                .partitions(5) // setup to split topic to 10 partitions
                .build();
    }

    @Bean
    public NewTopic omgJsonTopic() {
        return TopicBuilder.name("omgJson")
                .partitions(5) // setup to split topic to 10 partitions
                .build();
    }
}
