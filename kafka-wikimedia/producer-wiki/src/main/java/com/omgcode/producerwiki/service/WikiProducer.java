package com.omgcode.producerwiki.service;


import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;
import com.omgcode.producerwiki.common.AppConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class WikiProducer {

    private KafkaTemplate<String, String> kafkaTemplate;

    public WikiProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage() throws InterruptedException {

        // Read real time stream data from wikimedia, we use the event source
        // Create an Event source
        EventHandler eventHandler = new WikiChangesHandler(this.kafkaTemplate, AppConstant.WIKI_TOPIC);
        EventSource.Builder eventSourceBuilder = new EventSource.Builder(eventHandler, URI.create(AppConstant.WIKI_STREAM_URL));
        EventSource eventSource = eventSourceBuilder.build();

        // Get data
        eventSource.start();

        TimeUnit.MINUTES.sleep(10);

    }
}
