package com.omgcode.producerwiki.config;

import com.omgcode.producerwiki.common.AppConstant;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic topic(){
        return TopicBuilder
                .name(AppConstant.WIKI_TOPIC)
                .partitions(5)
                .build();
    }
}
