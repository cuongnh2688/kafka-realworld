package com.omgcode.producerwiki.common;

public class AppConstant {
    public final static String WIKI_TOPIC = "wikimedia_recentchange";

    public final static String WIKI_STREAM_URL = "https://stream.wikimedia.org/v2/stream/recentchange";
}
