package com.omgcode.producerwiki;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerWikiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProducerWikiApplication.class, args);
    }
}
