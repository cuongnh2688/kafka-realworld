package com.omgcode.producerwiki.controller;


import com.omgcode.producerwiki.service.WikiProducer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/wiki")
public class WikiProducerController {

    private WikiProducer wikiProducer;

    public WikiProducerController(WikiProducer wikiProducer) {
        this.wikiProducer = wikiProducer;
    }

    @PostMapping("/publish")
    public ResponseEntity<String> publish() throws InterruptedException {
        this.wikiProducer.sendMessage();
        return ResponseEntity.ok("Wiki media data publish successfully!");
    }
}
