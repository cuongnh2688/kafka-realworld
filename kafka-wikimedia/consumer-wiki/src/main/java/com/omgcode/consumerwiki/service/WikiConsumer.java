package com.omgcode.consumerwiki.service;

import com.omgcode.consumerwiki.common.AppConstant;
import com.omgcode.consumerwiki.entity.WikiDataEntity;
import com.omgcode.consumerwiki.repository.WikiDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WikiConsumer {

    @Autowired
    private WikiDataRepository wikiDataRepository;

    @KafkaListener(topics = AppConstant.WIKI_TOPIC, groupId = "myGroup")
    public void consume(String eventMessage) {
        log.info(String.format("Even message receive -> %s", eventMessage));
        this.wikiDataRepository.save(WikiDataEntity.builder().wikiData(eventMessage).build());
    }
}
