package com.omgcode.consumerwiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerWikiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerWikiApplication.class, args);
    }
}
