package com.omgcode.consumerwiki.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "WIKI_DATA")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WikiDataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Lob
    private String wikiData;
}
