package com.omgcode.consumerwiki.repository;

import com.omgcode.consumerwiki.entity.WikiDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WikiDataRepository extends JpaRepository<WikiDataEntity,Long> {
}
